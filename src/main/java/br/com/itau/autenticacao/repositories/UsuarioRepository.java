package br.com.itau.autenticacao.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.autenticacao.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
	
	public Optional<Usuario> findByNome(String nome);
}
